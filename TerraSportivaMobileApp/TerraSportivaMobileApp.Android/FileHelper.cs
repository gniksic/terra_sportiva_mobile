﻿using System;
using System.IO;
using Xamarin.Forms;
using TerraSportivaMobileApp.Droid;
using TerraSportivaMobileApp.Utils;

[assembly: Dependency(typeof(FileHelper))]
namespace TerraSportivaMobileApp.Droid
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
    }
}