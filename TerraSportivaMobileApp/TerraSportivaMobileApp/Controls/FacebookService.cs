﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TerraSportivaMobileApp.Models;

namespace TerraSportivaMobileApp.Controls
{
    public class FacebookService
    {
        public string FacebookAppId { get; set; }
        public string FacebookAppSecret { get; set; }
        public string RedirectUri { get; set; }
        public string AppScope { get; set; }

        public string GetLoginLink()
        {
            return "https://www.facebook.com/v2.10/dialog/oauth?client_id=" + this.FacebookAppId + "&redirect_uri=" + this.RedirectUri + "&scope=" + this.AppScope;
        }

        public FacebookAccessToken ParseToken(string token)
        {
            return JsonConvert.DeserializeObject<FacebookAccessToken>(token);
        }
        /// <summary>
        /// Exchange facebook returned oauth code for access token
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<string> GetAccessTokenWithCode(string code)
        {
            // Exchange the code for an access token
            Uri targetUri = new Uri("https://graph.facebook.com/oauth/access_token?client_id=" + this.FacebookAppId + "&client_secret=" + this.FacebookAppSecret + "&redirect_uri=" + this.RedirectUri + "&code=" + code);
            HttpClient client = new HttpClient();
            string content = "";

            try
            {
                HttpResponseMessage response = await client.GetAsync(targetUri, CancellationToken.None);
                if (response.IsSuccessStatusCode)
                {
                    content = await response.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            string token = content.Replace("access_token=", "");

            // Split the access token and expiration from the single string
            string[] combined = token.Split('&');
            string accessToken = combined[0];
            return accessToken;
        }

        /// <summary>
        /// Gets user info with access token
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public async Task<FacebookUser> Identify(string accessToken)
        {
            // Request the Facebook user information
            Uri targetUserUri = new Uri("https://graph.facebook.com/me?fields=id,email,first_name,last_name,gender,link&access_token=" + accessToken);
            HttpClient client = new HttpClient();
            string content = "";

            try
            {
                HttpResponseMessage response = await client.GetAsync(targetUserUri, CancellationToken.None);
                if (response.IsSuccessStatusCode)
                {
                    content = await response.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            // Deserialize and convert the JSON object to the Facebook.User object type
            return JsonConvert.DeserializeObject<FacebookUser>(content);
        }

        /// <summary>
        /// Gets facebook user info with oauth code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<FacebookUser> GetFacebookUserDataWithCode(string code)
        {
            var accessToken = await GetAccessTokenWithCode(code);
            return Identify(accessToken).Result;
        }
    }
}
