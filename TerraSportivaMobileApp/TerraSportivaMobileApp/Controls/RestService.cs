﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TerraSportivaMobileApp.Models;

namespace TerraSportivaMobileApp.Controls
{
    public class RestService
    {
        //HttpClient client;
        //public Gym[] GymItems { get; private set; }

        //public RestService()
        //{
        //    client = new HttpClient();
        //    client.MaxResponseContentBufferSize = 256000;
        //    InitList();
        //}

        //public async void InitList()
        //{
        //    GymItems = await GetGymData();
        //}

        public static async Task GetAllGymData(Action<IEnumerable<Gym>> action)
        {

            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            var url = "https://api.myjson.com/bins/1g9yz1";// "http://192.168.1.72:8888/gyms.json";

            try
            {
                HttpResponseMessage response = await client.GetAsync(url, CancellationToken.None);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    var gymList = JsonConvert.DeserializeObject<IEnumerable<Gym>> (content);
                    action(gymList);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }
    }
}
