﻿using SQLite.Net;
using SQLite.Net.Async;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerraSportivaMobileApp.Utils
{
    public interface ILocalDB
    {
            SQLiteAsyncConnection GetAsyncConnection();
            void CreateDbIfNotExist();
    }
}


