﻿using SQLite;
using System.Collections.Generic;
using System.Threading.Tasks;
using TerraSportivaMobileApp.Models;

namespace TerraSportivaMobileApp.Utils
{
    public class LocalDB
    {
        readonly SQLiteAsyncConnection database;
        bool isInitialised;

        public LocalDB(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<Gym>().Wait();
            isInitialised = InitializeDatabase().Result;
        }

        public Task<List<Gym>> GetItemsAsync()
        {
            return database.Table<Gym>().ToListAsync();
        }

        public Task<int> UpdateItemAsync(Gym updatedGym)
        {
            return database.UpdateAsync(updatedGym);
        }

        public Task<Gym> GetItemAsync(int id)
        {
            return database.Table<Gym>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(Gym item)
        {
            if (item.ID != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(Gym item)
        {
            return database.DeleteAsync(item);
        }

        public Task<bool> InitializeDatabase()
        {
            int inserted = -1;
            Gym[] initalItems = new Gym[]
            {
                new Gym("Gyms4You VMD", "Strojarska ul. 20","Zagreb", "Centre", "10000", "Croatia", GymType.PublicGym, "+45.8035432+15.989361", "gym1.jpg", null)
            };
            
            //[{"Name":"Gyms4You VMD","Address":"Strojarska ul. 20","City":"Zagreb","CityPart":"Centre","Zip":"10000","Country":"Croatia","GymType":"PublicGym","Geolocation":"+45.8035432+15.989361","ImageSource":"gym1.jpg"}
            //{"Name":"Gyms4You Dubrava","Address":"Dubrava 33","City":"Zagreb","CityPart":"Dubrava","Zip":"10000","Country":"Croatia","GymType":"RestrictedGym","Geolocation":"+45.8261512+16.0434607","ImageSource":"gym2.jpg"}
            //{"Name":"Gyms4You Bundek","Address":"Damira Tomljanovica Gavrana 11","City":"Zagreb","CityPart":"Novi Zagreb","Zip":"10000","Country":"Croatia","GymType":"PublicGym","Geolocation":"+45.7824332+15.9813501","ImageSource":"gym3.jpg"}]

            inserted = database.InsertAllAsync(initalItems).Result;
            if (inserted != -1)
            {
                return new Task<bool>(() => { return true; });
            }
            else
            {
                return new Task<bool>(() => { return false; });
            }
        }

    }
}
