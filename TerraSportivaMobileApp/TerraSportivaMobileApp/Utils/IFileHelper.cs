﻿namespace TerraSportivaMobileApp.Utils
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
