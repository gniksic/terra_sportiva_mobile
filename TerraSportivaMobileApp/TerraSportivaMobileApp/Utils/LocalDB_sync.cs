﻿using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerraSportivaMobileApp.Models;
using Xamarin.Forms;

namespace TerraSportivaMobileApp.Utils
{
    public class LocalDB_sync
    {
        private SQLiteAsyncConnection _connection;

        public LocalDB_sync()
        {
            _connection = DependencyService.Get<ILocalDB>().GetConnection();
            _connection.CreateTable<Gym>();
            _connection.CreateTable<Coach>();
        }

        // Gym CRUD

        #region Gym CRUD

        public IEnumerable<Gym> GetGyms()
        {
            return (from t in _connection.Table<Gym>()
                    select t).ToList();
        }

        public Gym GetGym(int id)
        {
            return _connection.Table<Gym>().FirstOrDefault(t => t.ID == id);
        }

        public void DeleteGym(int id)
        {
            _connection.Delete<Gym>(id);
        }

        public void AddGym(string name, string address, string city, string cityPart, string zip, string country, GymType type, string geoLocation, string imageSource, string[] tagList)
        {
            var newGym = new Gym(name, address, city, cityPart, zip, country, type, geoLocation, imageSource, tagList);

            _connection.Insert(newGym);
        }
        #endregion

        // Coach CRUD

        #region Coach CRUD

        public IEnumerable<Coach> GetCoaches()
        {
            return (from t in _connection.Table<Coach>()
                    select t).ToList();
        }

        public Coach GetCoache(int id)
        {
            return _connection.Table<Coach>().FirstOrDefault(t => t.ID == id);
        }

        public void DeleteCoach(int id)
        {
            _connection.Delete<Coach>(id);
        }

        public void AddCoach(string firstName, string lastName, string nickName, int experience, string[] sports)
        {
            var newCoach = new Coach(firstName, lastName, nickName, experience, sports);

            _connection.Insert(newCoach);
        }
        #endregion
    }
}
