﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TerraSportivaMobileApp.Views;
using TerraSportivaMobileApp.Utils;

// želimo da se XAML kompajlira u C# kod - kompaktnije je, validira XAML i nema runtime greška koje je teško debuggirati
[assembly: XamlCompilationAttribute(XamlCompilationOptions.Compile)]
//
namespace TerraSportivaMobileApp
{
    public partial class App : Application
    {
        public static bool IsUserLoggedIn { get; set; }

        static LocalDB database;

        public App()
        {

            InitializeComponent();

            //MainPage = new TerraSportivaMobileApp.MainPage();
            //MainPage = new NavigationPage(new MainPage());
            //MainPage = new QuestionPage();
            //MainPage = new SideMenuPage();
            //MainPage = new GymListPage();

            //MainPage = new NavigationPage(new GymListPage());
            MainPage = new SideMenuPage();
        }
        public static LocalDB Database
        {
            get
            {
                if (database == null)
                {
                    database = new LocalDB(DependencyService.Get<IFileHelper>().GetLocalFilePath("TodoSQLite.db3"));
                }
                return database;
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            //LocalDB_sync.Instance.CreateDbIfNotExist();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
