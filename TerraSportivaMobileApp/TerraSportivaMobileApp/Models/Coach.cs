﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerraSportivaMobileApp.Models
{
    class Coach
    {
        // private properties

        private int id;

        private string firstName;

        private string lastName;

        private string nickName;

        private int experience;

        //private string[] sports;

        // public properties

        [PrimaryKey, AutoIncrement]
        public int ID
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }
        public string FirstName
        {
            get
            {
                return this.firstName;
            }
            set
            {
                this.firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                this.lastName = value;
            }
        }

        public string NickName
        {
            get
            {
                return this.nickName;
            }
            set
            {
                this.nickName = value;
            }
        }

        public int Experience
        {
            get
            {
                return this.experience;
            }
            set
            {
                this.experience = value;
            }
        }

        //public string[] Sports
        //{
        //    get
        //    {
        //        return this.sports;
        //    }
        //    set
        //    {
        //        this.sports = value;
        //    }
        //}

        // TBD: How to link a coach to a gym; is this optional or obligatory

        public Coach()
        {
        }

        public Coach(string firstName, string lastName, string nickName, int experience) //, string[] sports)
        {
            FirstName = firstName;
            LastName = lastName;
            NickName = nickName;
            Experience = experience;
        }
    }
}
