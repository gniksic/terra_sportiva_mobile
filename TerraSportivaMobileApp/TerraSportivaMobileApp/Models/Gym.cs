﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace TerraSportivaMobileApp.Models
{
    public class Gym
    {
        #region Private fields

        // For the database
        private int id;

        // Private main fields
        private string name;
        private string address;
        private string city;
        private string cityPart;
        private string zip;
        private string country;
        private GymType type;
        private string geoLocation;
        private string imageSource;
        private string[] tagList;

        // Private derived fields
        private string cityFull;
        private string addressFull;

        // Private functionality fields

        private bool isInactiva;

        #endregion

        #region Public fields

        // Public primary fields
        [PrimaryKey, AutoIncrement]
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string Address
        {
            get { return this.address; }
            set
            {
                this.address = value;
                this.addressFull = value + ", " + zip + " " + this.city;
            }
        }
        public string City
        {
            get { return this.city; }
            set
            {
                this.city = value;
                this.cityFull = value + "(" + this.cityPart + ")";
            }
        }
        public string CityPart
        {
            get { return this.cityPart; }
            set
            {
                this.cityPart = value;
                this.cityFull = this.city + "(" + value + ")";
            }
        }
        public string Zip
        {
            get { return this.zip; }
            set { this.zip = value; }
        }
        public string Country
        {
            get { return this.country; }
            set { this.country = value; }
        }
        [Ignore]
        public GymType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }
        public string GeoLocation
        {
            get { return this.geoLocation; }
            set { this.geoLocation = value; }
        }

        public string ImageSource
        {
            get
            {
                return this.imageSource;
            }
            set
            {
                this.imageSource = value;
                //this.imageSource = "images/" + value;
            }
        }
        [Ignore]
        public string[] TagList
        {
            get
            {
                return this.tagList;
            }
            set
            {
                this.tagList = value;
            }
        }

        // Public derived fields
        public string CityFull
        {
            get { return this.cityFull; }
        }
        public string AddressFull
        {
            get { return this.addressFull; }
        }

        // Public functionality fields
        public bool IsInactive
        {
            get
            {
                return this.isInactiva;
            }
            set
            {
                this.isInactiva = value;
            }
        }

        #endregion

        // Constructors

        // Empty one required for the database
        public Gym()
        {
        }
        public Gym(string name, string address, string city, string cityPart, string zip, string country, GymType type, string geoLocation, string imageSource)
        {
            Name = name;
            Address = address;
            City = city;
            CityPart = cityPart;
            Zip = zip;
            Country = country;
            Type = type;
            GeoLocation = geoLocation;
            ImageSource = imageSource;
        }
        public Gym(string name, string address, string city, string cityPart, string zip, string country, GymType type, string geoLocation, string imageSource, string[] tagList)
        {
            Name = name;
            Address = address;
            City = city;
            CityPart = cityPart;
            Zip = zip;
            Country = country;
            Type = type;
            GeoLocation = geoLocation;
            ImageSource = imageSource;
            TagList = tagList;
            IsInactive = isInactiva;
        }

        // Methods

        public override string ToString()
        {
            return string.Format("[Gym: ID={0}, Name={1}, Location={2}]", ID, Name, CityFull);
        }
    }
}
