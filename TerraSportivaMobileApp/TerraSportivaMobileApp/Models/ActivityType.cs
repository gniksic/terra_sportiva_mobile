﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerraSportivaMobileApp.Models
{
    public enum ActivityType
    {
        Group = 0,
        Individual = 1,
        Couple = 2
    }
}
