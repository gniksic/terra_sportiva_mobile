﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerraSportivaMobileApp.Models
{
    public enum GymType
    {
        PublicGym = 0,
        RestrictedGym = 1,
        SchoolGym = 2,
        DanceGym = 3
    }
}
