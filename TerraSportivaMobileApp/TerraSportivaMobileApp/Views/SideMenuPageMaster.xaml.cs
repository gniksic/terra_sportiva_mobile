﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TerraSportivaMobileApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SideMenuPageMaster : ContentPage
    {
        public ListView ListView;

        public SideMenuPageMaster()
        {
            InitializeComponent();
            BindingContext = new SideMenuPageMasterViewModel();
            ListView = MenuItemsListView;
        }

        class SideMenuPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<SideMenuPageMenuItem> MenuItems { get; set; }

            public SideMenuPageMasterViewModel()
            {
                MenuItems = new ObservableCollection<SideMenuPageMenuItem>(new[]
                {
                    new SideMenuPageMenuItem { Id = 0, Title = "Home Page with API", TargetType = typeof(GymListPage)},
                    new SideMenuPageMenuItem { Id = 1, Title = "Local Database", TargetType = typeof(SideMenuPageDetail)},
                    new SideMenuPageMenuItem { Id = 2, Title = "Map Display Page", TargetType = typeof(MapViewPage)},
                    //new SideMenuPageMenuItem { Id = 3, Title = "Coupon List", TargetType = typeof(CouponPage)}
                });
            }
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}