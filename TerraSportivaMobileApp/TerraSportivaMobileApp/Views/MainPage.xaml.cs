﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TerraSportivaMobileApp.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            if (!App.IsUserLoggedIn)
            {
                OpenLoginPage();
            }
        }

        async void OpenLoginPage()
        {
            await Navigation.PushAsync(new LoginPage());
        }

        async void btnSubmit_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new GymListPage(), true);
        }
    }
}
