﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TerraSportivaMobileApp.Views
{

    public class SideMenuPageMenuItem
    {
        public SideMenuPageMenuItem()
        {
            TargetType = typeof(SideMenuPageDetail);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}