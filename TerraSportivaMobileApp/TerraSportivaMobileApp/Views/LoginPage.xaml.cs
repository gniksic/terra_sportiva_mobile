﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TerraSportivaMobileApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        private void btnLogin_Clicked(object sender, EventArgs e)
        {
            App.IsUserLoggedIn = true;
            Navigation.PopAsync();
        }
        private void btnFBLogin_Clicked(object sender, EventArgs e)
        {

        }

        private void btnSignUp_Clicked(object sender, EventArgs e)
        {

        }
    }
}