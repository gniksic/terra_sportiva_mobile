﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TerraSportivaMobileApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SideMenuPage : MasterDetailPage
    {
        public SideMenuPage()
        {
            InitializeComponent();
            
            if (Device.RuntimePlatform == Device.Windows)
            {
                Master.Icon = "menu-icon.png";
                MasterBehavior = MasterBehavior.Popover; // Added this line of code
            }
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as SideMenuPageMenuItem;
            if (item == null)
                return;

            var page = (Page)Activator.CreateInstance(item.TargetType);
            page.Title = item.Title;

            Detail = new NavigationPage(page);
            IsPresented = false;

            MasterPage.ListView.SelectedItem = null;
        }
    }
}