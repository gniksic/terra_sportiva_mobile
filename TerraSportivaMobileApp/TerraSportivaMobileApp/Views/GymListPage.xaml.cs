﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TerraSportivaMobileApp.Controls;
using TerraSportivaMobileApp.Models;
using System.Net.Http;
using System.Threading;
using System.Diagnostics;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TerraSportivaMobileApp.Views
{
    public partial class GymListPage : ContentPage
    {
        //public ListView ListView;
        //public Gym[] GymItems;
        public GymListPage()
        {
            InitializeComponent();
            BindingContext = new GymListViewModel();
            //ListView = GymListView;
        }

        //public static async Task<ObservableCollection<Gym>> GetGymList()
        //{
        //    ObservableCollection<Gym> GymItems = null;
        //    var client = new HttpClient();
        //    client.MaxResponseContentBufferSize = 256000;
        //    var url = "http://192.168.1.72:8888/gyms.json";
        //    try
        //    {
        //        HttpResponseMessage response = await client.GetAsync(url, CancellationToken.None);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            string content = await response.Content.ReadAsStringAsync();
        //            GymItems = JsonConvert.DeserializeObject<ObservableCollection<Gym>>(content);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex);
        //    }
        //    return GymItems;
        //}
    }
    
    public class GymListViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Gym> gymItems;
        public ObservableCollection<Gym> GymItems
        {
            get
            {
                return gymItems;
            }
            set
            {
                gymItems = value;
                OnPropertyChanged();
            }
        }
        //private DataStore store;

        public GymListViewModel()
        {
            GymItems = new ObservableCollection<Gym>();
            //{
            //    new Gym("test", "test", "test", "test", "test", "test", GymType.SchoolGym, "test", "gym1.jpg")
            //};

            GetAllGymItems();

            //store = new DataStore();
            //var gymsFromJson = store.GetGymsFromMockup();
            //if (gymsFromJson.Length == 0)
            //{
            //    GymItems = new ObservableCollection<Gym>(new[]
            //    {
            //    new Gym("test gym", "adresa", "grad",  "diograda", "0000", "drzava", GymType.PublicGym, "+45+16", "0.jpg"),
            //    new Gym("test gym 2", "adresa2", "grad2",  "diograda2", "0000a", "drzava2", GymType.RestrictedGym, "+44+16", "0a.jpg")
            //    });
            //}
            //else
            //{
            //    GymItems = new ObservableCollection<Gym>(gymsFromJson);
            //}
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        public async void GetAllGymItems()
        {
            await RestService.GetAllGymData(list =>
            {
                foreach (Gym gym in list)
                    GymItems.Add(gym);
            });
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}